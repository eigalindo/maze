const board = document.getElementById("board")
const winCon = document.getElementById("winCon")
const player = document.getElementById("player")
const map = [ 
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let xcoordinate
let ycoordinate

for(let rowIndex = 0; rowIndex < map.length; rowIndex++){
    const row=document.createElement("div");
    row.classList.add("row")
    for(let cellIndex = 0; cellIndex<map[rowIndex].length; cellIndex++ ){
        const cell=document.createElement("div");
        cell.classList.add("cell")
        cell.dataset.rowIndex=rowIndex;
        cell.dataset.cellIndex=cellIndex;
        row.appendChild(cell)
        switch (map[rowIndex][cellIndex]){
            case "W":
                cell.classList.add("wall")
                break

            case "S":
                cell.id = "start"
                xcoordinate = cellIndex
                ycoordinate = rowIndex
                break

        }
    }
board.appendChild(row);
}
function movePlayer(event)  {
        switch (event.key){
            case "ArrowUp":
                    if (map[ycoordinate - 1][xcoordinate] === " "){
                        let moveRight = document.querySelector("[data-row-index='" + (ycoordinate - 1) + "'][data-cell-index='" + xcoordinate + "']")
                        console.log(moveRight)
                        moveRight.appendChild(player)
                        ycoordinate = ycoordinate - 1
                    }
                break
            
            case "ArrowDown":
                    if (map[ycoordinate + 1][xcoordinate] === " "){
                        let moveRight = document.querySelector("[data-row-index='" + (ycoordinate + 1) + "'][data-cell-index='" + xcoordinate + "']")
                        console.log(moveRight)
                        moveRight.appendChild(player)
                        ycoordinate = ycoordinate + 1
                    }
                break
    
            case "ArrowLeft":
                if (map[ycoordinate][xcoordinate - 1] === " "){
                    let moveRight = document.querySelector("[data-row-index='" + ycoordinate + "'][data-cell-index='" + (xcoordinate - 1) + "']")
                    console.log(moveRight)
                    moveRight.appendChild(player)
                    xcoordinate = xcoordinate - 1
                }
                break
    
            case "ArrowRight":
                console.log("Y COORD:", ycoordinate, "\nX COORD:", xcoordinate)
                if (map[ycoordinate][xcoordinate + 1] === " "){
                    let moveRight = document.querySelector("[data-row-index='" + ycoordinate + "'][data-cell-index='" + (xcoordinate + 1) + "']")
                    console.log(moveRight)
                    moveRight.appendChild(player)
                    xcoordinate = xcoordinate + 1
                }
                else if (map[ycoordinate][xcoordinate + 1] === "F"){
                    let moveRight = document.querySelector("[data-row-index='" + ycoordinate + "'][data-cell-index='" + (xcoordinate + 1) + "']")
                    console.log(moveRight)
                    moveRight.appendChild(player)
                    xcoordinate = xcoordinate + 1
                    winCon.style.zIndex = "1"
                }
                break
        }
}
document.addEventListener("keydown", movePlayer)
document.getElementById("start").appendChild(player)